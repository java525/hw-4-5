package ua.hillel;

import java.util.ArrayList;
import java.util.Scanner;

public class Service {

    private static final int countOfPoints = 10;

    public void execute() {
        Scanner scanner = new Scanner(System.in);
        UserUi ui = new UserUi(scanner);

        Point[] points = new Point[countOfPoints];
        for (int i = 0; i < points.length; i++) {
            points[i] = ui.getCoordinatesOfPoint(i + 1);
        }

        Circle circle = new Circle();
        circle.setCenterOfCircle(ui.getCoordinatesCircleCenter());
        circle.setRadius(ui.getCircleRadius());

        ArrayList listOfPointsInsideTheCircle = new ArrayList<>();

        for (int i = 0; i < points.length; i++) {
            if (circle.checkIfPointIsInCircle(points[i])) {
                listOfPointsInsideTheCircle.add(points[i]);
            }
        }

        ui.printPointsInTheCircle(listOfPointsInsideTheCircle);
    }
}
