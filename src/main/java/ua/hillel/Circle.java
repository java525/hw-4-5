package ua.hillel;

public class Circle {
    private Point centerOfCircle;
    private double radius;

    public Circle() {
    }

    public boolean checkIfPointIsInCircle(Point point) {
        double distance = point.countDistanceBetweenTwoPoints(centerOfCircle);
        return radius > distance;
    }

    public Point getCenterOfCircle() {
        return centerOfCircle;
    }

    public void setCenterOfCircle(Point centerOfCircle) {
        this.centerOfCircle = centerOfCircle;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
