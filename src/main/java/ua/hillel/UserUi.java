package ua.hillel;

import java.util.ArrayList;
import java.util.Scanner;

public class UserUi {
    private Scanner scanner;

    public UserUi(Scanner scanner) {
        this.scanner = scanner;
    }

    public Point getCoordinatesOfPoint(int number) {
        System.out.println("Enter coordinates of point #" + number);
        double x = scanner.nextDouble();
        scanner.nextLine();
        double y = scanner.nextDouble();
        scanner.nextLine();

        return new Point(x, y);
    }

    public Point getCoordinatesCircleCenter() {
        System.out.println("Enter coordinates of the center of the circle: ");
        double x = scanner.nextDouble();
        scanner.nextLine();
        double y = scanner.nextDouble();
        scanner.nextLine();

        return new Point(x, y);
    }

    public double getCircleRadius() {
        System.out.println("Enter circle radius: ");
        double radius = scanner.nextDouble();

        return radius;
    }

    public void printPointsInTheCircle(ArrayList list) {
        System.out.println("Points that are in the circle: ");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }
}
