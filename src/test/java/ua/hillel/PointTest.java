package ua.hillel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PointTest {

    @Test
    public void shouldCountDistance() {
        Point point1 = new Point(5, 5);
        Point point2 = new Point(10, 10);

        double distanceActual = point1.countDistanceBetweenTwoPoints(point2);
        double distanceExpected = 7.0710678118654755;

        assertEquals(distanceExpected, distanceActual);

    }
}
