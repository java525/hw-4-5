package ua.hillel;

import org.junit.jupiter.api.Test;
import java.util.InputMismatchException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class UserUiTest {

    private UserUi ui;
    private static final int countOfPoints = 5;

    @Test
    public void shouldCreatePoint() {
        ui = new UserUi(new Scanner("5\n5\n"));

        Point actualPoint = ui.getCoordinatesOfPoint(countOfPoints);
        Point expectedPoint = new Point(5, 5);

        assertEquals(expectedPoint, actualPoint);
    }

    @Test
    public void shouldCreateCenterOfCircle() {
        ui = new UserUi(new Scanner("3\n4\n"));

        Point actualPoint = ui.getCoordinatesCircleCenter();
        Point expectedPoint = new Point(3, 4);

        assertEquals(expectedPoint, actualPoint);
    }

    @Test
    public void shouldGetRadius() {
        ui = new UserUi(new Scanner("10\n"));

        double actualRadius = ui.getCircleRadius();

        assertEquals(10.00, actualRadius);
    }

    @Test
    void shouldThrowInputMismatchExceptionForLetters() {
        ui = new UserUi(new Scanner("F\n"));

        assertThrows(InputMismatchException.class, () -> ui.getCircleRadius());
        assertThrows(InputMismatchException.class, () -> ui.getCoordinatesOfPoint(countOfPoints));
        assertThrows(InputMismatchException.class, () -> ui.getCoordinatesCircleCenter());
    }
}